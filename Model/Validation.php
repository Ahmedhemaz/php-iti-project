<?php 


$db = new DbHandler("users");


$sign_up_validation = new SignUpValidation($db);

$sign_in_validation = new LoginValidation($db);

$show_user = new ShowUser($db);

$sign_up_errors = array();

$sign_in_errors = array();
 

if(isset($_POST["signup"])){

    $sign_up_errors = $sign_up_validation->check_sign_up();

	if(empty($sign_up_errors)){

        $sign_up_validation->insert_sign_up_database();
        
	}


}
elseif(isset($_POST["signin"])){

    $sign_in_errors = $sign_in_validation->check_sign_in();

    if(empty($sign_in_errors)){

        $sign_in_validation->sign_user_in_session();
        
	}

}




?>