<?php 


class LoginValidation{

    private $_db;

    private $_sign_in_errors;



    public function __construct($data_base){

        $this->_db = $data_base;

    }


    public function check_sign_in(){
        $this->check_username_exist();

        //if the username doesn't exit then i don't need to check for any other validation
        if(empty($this->_sign_in_errors)){
            $this->check_password();
        }

        return $this->_sign_in_errors;


    }


	//if the username already exists in database
    private function check_username_exist(){

		if(!$this->_db->sign_up_check($_POST["username"])){
			$this->_sign_in_errors["username"] = "User doesn't exit";
		}

    }


	//if the password is incorrect
    private function check_password(){
		if(!$this->_db->sign_in_check($_POST["username"] , $_POST["password"])){
			$this->_sign_in_errors["password"] = "Password is incorrect";

		}

    }

    //if the username and password are correct then get his id from database and insert it to session
    public function sign_user_in_session(){

        $id = $this->_db->show_data_by_username($_POST["username"]);
        $_SESSION["user_id"] = $id[0]["id"];

        //if id equals to one that means u are admin
        if($id[0]["id"] === 1){
            $_SESSION["is_admin"] = true;


        }
        else{
            $_SESSION["is_admin"] = false;


        }

    }



}



?>