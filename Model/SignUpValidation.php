<?php 


class SignUpValidation{

    private $_db;

    private $_sign_up_errors;

    private $_image_name;

    private $_cv_name;

    private $_image_file_extension;

    private $_cv_file_extension;

    private $_target_image_path;

    private $_target_cv_path;

    public function __construct($data_base){

        $this->_db = $data_base;

    }


    public function check_sign_up(){

        
        $this->get_image_name();
        $this->get_image_extension();
        $this->get_target_image_path();

        $this->get_cv_name();
        $this->get_cv_extension();
        $this->get_target_cv_path();

        $this->check_first_name();
        $this->check_last_name();
        $this->check_job();
        $this->check_email();

        $this->check_username();
        $this->check_password();
        $this->check_repeat_password();

        $this->check_image();
        $this->check_cv();

        //try to upload files if only they passed the first check
        if(empty($this->_sign_up_errors)){

            $this->check_image_upload();
            $this->check_cv_upload();
        }

        //check the user in the database if only all the other validations were correct
        if(empty($this->_sign_up_errors)){

            $this->check_username_exist();

        }


        return $this->_sign_up_errors;



    }

    // if the FirstName doesn't exists or  it is  empty then add error to the array
    private function check_first_name(){

		if(!isset($_POST["fname"]) || empty($_POST["fname"])){
			$this->_sign_up_errors["fname"] = "FirstName Can't be Empty";

		}

    }

	// if the name doesn't exists or  it is  empty then add error to the array
    private function check_last_name(){

		if(!isset($_POST["lname"]) || empty($_POST["lname"])){
			$this->_sign_up_errors["lname"] = "LastName Can't be Empty";

		}

    }

	// if the job is invalid
    private function check_job(){

		if(!isset($_POST["job"]) || empty($_POST["job"])){
			$this->_sign_up_errors["job"]  = "Job Can't be Emprty";
		
		}

    }

	// if the email is invalid or the filter of email is incorrect 
    private function check_email(){

		if(!isset($_POST["email"]) || empty($_POST["email"])){
			$this->_sign_up_errors["email"] = "Email Can't be Empty";
        }
        else if(!filter_var($_POST["email"] , FILTER_VALIDATE_EMAIL)){

            $this->_sign_up_errors["email"] = "Email Format InCorrect";

        }

    }
    
	// if the username is invalid
    private function check_username(){

		if(!isset($_POST["username"]) || empty($_POST["username"])){
			$this->_sign_up_errors["username"] = "UserName Can't be Empty";
		}

    }


    private function check_password(){

		//if the password is invalid or the length is not correct
		if(!isset($_POST["password"]) || empty($_POST["password"])  ){
			$this->_sign_up_errors["password"] = "Password Can't be Empty";
        }
        elseif(strlen($_POST["password"]) < __password_min__){
			$this->_sign_up_errors["password"] = "Password Must be More Than".__password_min__."characters";


        }
        elseif(strlen($_POST["password"]) > __password_max__){
			$this->_sign_up_errors["password"] = "Password Must be Less Than ".__password_max__."characters";


        }

    }

    private function check_repeat_password(){

		//if the two password don't match
		if(!isset($_POST["repeat-password"]) || empty($_POST["repeat-password"])){
			$this->_sign_up_errors["repeat-password"] = "Repeat Password Can't be Empty";
        }
        elseif($_POST["password"] != $_POST["repeat-password"]){

			$this->_sign_up_errors["repeat-password"] = "Passwords don't Match";

        }

    }

	//get the name of the image
    private function get_image_name(){
        $this->_image_name = basename($_FILES["image"]["name"]);
        
        return $this->_image_name;

    }

    //get image file extension
    private function get_image_extension(){

        $this->_image_file_extension = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);

        return $this->_image_file_extension; 

    }

    //get image path to upload
    private function get_target_image_path(){

        $this->_target_image_path = __image_dir__.$this->_image_name;

        return $this->_target_image_path;
    }

    //get the name of the cv
    private function get_cv_name(){
        $this->_cv_name = basename($_FILES["cv"]["name"]);
        
        return $this->_cv_name;

    }


    //get image file extension
    private function get_cv_extension(){

        $this->_cv_file_extension = pathinfo($_FILES["cv"]["name"], PATHINFO_EXTENSION);

        return $this->_cv_file_extension; 

    }

    //get image path to upload
    private function get_target_cv_path(){

        $this->_target_cv_path = __cv_dir__.$this->_cv_name;

        return $this->_target_cv_path;
    }

    //validate image
    private function check_image(){

        //if the image wasn't uploaded 
		if(!file_exists($_FILES["image"]["tmp_name"])){
			$this->_sign_up_errors["image"] = "Image Wasn't uploaded";

		}
		//if the image is not jpg
		else if($this->_image_file_extension !== __image_extension__){

			$this->_sign_up_errors["image"] = "Image isn't jpg";
	
		}
		//if it's bigger than one MB
		else if(($_FILES["image"]["size"] > __image_max_size__)){

			$this->_sign_up_errors["image"] = "Image Size is too large";

		}
    }


    //validate cv
    private function check_cv(){

        //if the cv wasn't uploaded
        if(!file_exists($_FILES["cv"]["tmp_name"])){
            $this->_sign_up_errors["cv"] = "CV Wasn't uploaded";

        }
        //if the cv is not pdf
        else if($this->_cv_file_extension !== __cv_extension__){
            $this->_sign_up_errors["cv"] = "PDF is not a pdf";
        }
        //if it's bigger than one MB
        else if(($_FILES["cv"]["size"] > __pdf_max_size__)){

            $this->_sign_up_errors["cv"] = "PDF Size is too large";

        }

    }

    //check if the image was uploaded
    private function check_image_upload(){
        if(!move_uploaded_file($_FILES["image"]["tmp_name"] , $this->_target_image_path)){

            $this->_sign_up_errors["image"] = "Couldn't Upload Image";
        }

    }


    //check if the cv was uploaded
    private function check_cv_upload(){
        if(!move_uploaded_file($_FILES["cv"]["tmp_name"] , $this->_target_cv_path)){

            $this->_sign_up_errors["cv"] = "Couldn't Upload CV";

        }

    }

	//if the username already exists in database
    private function check_username_exist(){

		if($this->_db->sign_up_check($_POST["username"])){

            $this->_sign_up_errors["username"] = "UserName Already Exists";
        }

    }


    public function insert_sign_up_database(){

        $dataArray = array ( "username" => $_POST['username'], "password" => $_POST['password'],
        "firstname" => $_POST['fname'], "lastname" => $_POST['lname'],
            "job" => $_POST['job'],"email" => $_POST['email'],
        "image" =>$this->_image_name, "cv" => $this->_cv_name
        );
        $dataKeyArray=  ["username","password","firstname","lastname" , "job" ,"email","image_name", "cv_name"];
        $this->_db->insertData($dataArray,$dataKeyArray);
    }



}



?>