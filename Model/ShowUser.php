<?php 


class ShowUser{

    private $_db;

    private $_user_id;

    private $_user_data;

    private $_user_name;

    private $_user_job;

    private $_user_email;

    private $_user_image;

    private $_user_cv;




    public function __construct($data_base){

        $this->_db = $data_base;

    }

    //get the id of the user
    public function get_user_id($id){
        $this->_user_id = $id;

        $this->get_user_data($this->_user_id);

        return $this->_user_id;

    }

    //get data from dataBase
    public function get_user_data($id){
        $this->_user_data = $this->_db->show_data_by_id($id);

        return $this->_user_data;

    }

    public function get_user_name($data){
        $this->_user_name = $data[0]['firstname']."  ".$data[0]['lastname'];

        return  $this->_user_name;

    }

    public function get_user_job($data){
        $this->_user_job = $data[0]['job'];

        return  $this->_user_job;
        
    }

    public function get_user_email($data){

        $this->_user_email = $data[0]['email'];

        return  $this->_user_email;        
    }

    public function get_user_image($data){
        $this->_user_image = __image_dir__.$data[0]['image_name'];

        return  $this->_user_image;
        
    }

    public function get_user_cv($data){
        $this->_user_cv = __cv_dir__.$data[0]['cv_name'];

        return  $this->_user_cv;

    }



}



?>