<?php

class DbHandler{


    private $_mysqli;
    private $_table;


    /*
     * database handler constructor
     * pass your table name and HOST,USER,PASS,DBNAME from config file to connect to your database
     * then will create connection to database
     *
     * */

    public function __construct($table){

        $this->_table = $table;
        $this->connect();
    }


    /*
     * create connection to database
     * change HOST,USER,PASS,DBNAME from config file to connect to your database
     *
     *
     * */

    public function connect(){

        $connection = new mysqli(__HOST__, __USER__, __PASS__, __DB__);
        if ($connection) {
            //echo "connection done";
            $this->_mysqli = $connection;
            return true;
        } else {
            return false;
        }
    }



    /*
     * TODOLIST
     *
     * insert data to database with param
     * SQL Injection
     * hashing password before inserting into database
     *
     * */


    public function insertData($dataArray,$keyArray){

        $this->connect();
        $valuesArray = array_fill(0,count($dataArray),'?');

        $stmt = $this->_mysqli->prepare('INSERT INTO  '.$this->_table.' ('
            .implode(", ", $keyArray).') VALUES ('
            .implode(", ",$valuesArray).')');

        $password = password_hash($dataArray['password'] , PASSWORD_DEFAULT);

        $stmt->bind_param("ssssssss",$dataArray['username'],$password,$dataArray['firstname'],$dataArray['lastname'] ,
            $dataArray['job'],$dataArray['email'],$dataArray['image'], $dataArray['cv']);

        $stmt->execute();

        $stmt->close();
        $this->disconnect();


    }

    /*
     * retrieve all user data with database id
     *
     * */


    public function show_data_by_id($id){

        $this->connect();
        $arr = [];
        $stmt = $this->_mysqli->prepare("SELECT * FROM $this->_table WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $arr[] = $row;
        }
        $this->disconnect();
        if(!$arr) exit('No rows');
        //var_export($arr);
        $stmt->close();

        return $arr;
    }


    /*
     * retrieve all user data with database id
     *
     * */

    public function show_data_by_username($username){

        $this->connect();
        $stmt = $this->_mysqli->prepare("SELECT id FROM $this->_table WHERE username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $id = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        $this->disconnect();
        return $id;
    }


    /*
     * get the data of all suers
     *
     *
     * */

    public function show_all_data($start_position = 0){

        $this->connect();
        $arr = [];
        $stmt = $this->_mysqli->prepare("SELECT * FROM $this->_table limit ? , ?");
        $record = __RECORDS_PER_PAGE__;
        $stmt->bind_param("ii", $start_position , $record);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $arr[] = $row;
        }
        //if(!$arr) exit('No rows');
        //var_export($arr);
        $stmt->close();
        $this->disconnect();
        return $arr;
    }


    /*
     *  validate signIn input with database
     *
     * */

    public function sign_in_check($username,$password){

        $this->connect();
        $stmt = $this->_mysqli->prepare("SELECT * FROM $this->_table WHERE username = ? ");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $arr = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $this->disconnect();
        if(!$arr){
            //echo "Does Not Exist";
            return false;
        }

        //check the hashed password
        if(!password_verify($password , $arr[0]["password"])){
            return false;
        }


        $stmt->close();
        return true;

    }


    /*
     *
     * signUp validation of user name
     *
     * */

    public function sign_up_check($username){

        $this->connect();
        $stmt = $this->_mysqli->prepare("SELECT * FROM $this->_table WHERE ( username = ? )");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $arr = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $this->disconnect();
        if(!$arr){
            //echo "Does Not Exist";
            return false;
        }

        $stmt->close();
        return true;

    }



    /*
     * data must be string to bind it
     * $dataArray must be Assoc Array so we can take key which will be named as column name in database
     * $id user id to be used with where
     *
     * */

    public function updateAll($dataArray,$id){

        foreach ($dataArray as $key=>$value){
            $this->updateData($value,$id,$key);
        }


    }

    /*
     * update any field with any value
     * can be used to edit single field
     * key should be the name of column in database
     *
     * */

    public function updateData($value, $id, $key){


        $this->connect();
        $stmt = $this->_mysqli->prepare("UPDATE $this->_table SET $key = ? WHERE id = ?");
        $stmt->bind_param("si", $value,$id);
        $stmt->execute();
        $stmt->close();
        $this->disconnect();

    }

    /*
     *
     * delete record in database
     * $id -> user id
     *
     * */

    public function deleteRecord($id){


        $this->connect();
        $stmt = $this->_mysqli->prepare("DELETE FROM $this->_table WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        echo "delete $id Record done";
        $stmt->close();
        $this->disconnect();

    }

    /*
     * search for record using any value
     * $key is database column name
     * */

    public function search($value , $key){

        $this->connect();
        $search = "%{$value}%";
        $stmt = $this->_mysqli->prepare("SELECT * FROM $this->_table WHERE $key LIKE ?");
        $stmt->bind_param("s", $search);
        $stmt->execute();
        $arr = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if(!$arr) exit('No rows');
        print_r($arr);
        $stmt->close();
        $this->disconnect();
        return $arr;

    }

    public function disconnect(){
        mysqli_close($this->_mysqli);
    }

}