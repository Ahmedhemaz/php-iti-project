<?php
session_start();
session_regenerate_id();



require_once("autoload.php");
require_once("Model/Validation.php");


define("_ALLOW_ACCESS", 1);



//********************************************//
//Routing

if (isset($_GET["logout"])) {

    require_once("Views/logout.php");
}
elseif (isset($_SESSION["user_id"]) && isset($_SESSION["is_admin"]) && $_SESSION["is_admin"] === true) {

    //admin views should be required here
    if(isset($_GET["id"])){

        require_once("Views/admin/user.php");

    }else{

        require_once("Views/admin/users.php");

    }

} elseif (isset($_SESSION["user_id"]) && isset($_SESSION["is_admin"]) && $_SESSION["is_admin"] === false) {

    //members views should be required here


    if(isset($_GET["edit"])){

        require_once("Views/member/edit_my_profile.php");
    }
    elseif(isset($_GET["about"])){

        require_once("Views/member/view_my_profile.php");
    }
    else{
        
        require_once("Views/member/view_my_profile.php");


    }

} else {

    //public views should be required here
     if(isset($_POST["isSignup"]) || !empty($sign_up_errors)){

        require_once("Views/public/signup.php");



    }else{

        require_once("Views/public/login.php");


    } 


}

//********************************************//