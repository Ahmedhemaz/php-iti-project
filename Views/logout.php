<?php
    session_destroy();

    defined("_ALLOW_ACCESS") or die("Access Not Allowed");

    header('location:index.php');

?>