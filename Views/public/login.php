<?php 

	defined("_ALLOW_ACCESS") or die("Access Not Allowed");



?>




<!DOCTYPE html>
<html lang="en">
<head>
	<title></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="Assets/images/icons/favicon.ico"/>

	<link rel="stylesheet" type="text/css" href="Assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/main.css">
</head>
<body style="background-color: #999999;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="login100-more" style="background-image: url('Assets/images/bg-01.jpg');"></div>

			<div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
				<form class="login100-form validate-form" method = "POST" enctype = "multipart/form-data" action = "<?php echo $_SERVER["PHP_SELF"]; ?>">
					<span class="login100-form-title p-b-59">
						Sign In
					</span>


					

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Username</span>
						<input class="input100" type="text" name="username" placeholder="Username...">
						<span class="focus-input100"></span>
						<?php
							if(isset($sign_in_errors["username"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_in_errors["username"]."</font>"."</span>";
							}
						?>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" placeholder="*************">
						<span class="focus-input100"></span>
						<?php
							if(isset($sign_in_errors["password"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_in_errors["password"]."</font>"."</span>";
							}
						?>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button type = "submit" name = "signin" class="login100-form-btn">
								Sign In
							</button>
						</div>

						<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button type = "submit" name = "isSignup" class="login100-form-btn" id ="sign">
								Sign Up
							</button>
						</div>

					</div>

					</div>


				</form>


			</div>
		</div>
	</div>
	


</body>
</html>


