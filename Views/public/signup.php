<?php

	defined("_ALLOW_ACCESS") or die("Access Not Allowed");

?>






<!DOCTYPE html>
<html lang="en">
<head>
	<title></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="Assets/images/icons/favicon.ico"/>

	<link rel="stylesheet" type="text/css" href="Assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/component.css" />


</head>
<body style="background-color: #999999;">

	<div class="limiter">
		<div class="container-login100">
			<div class="login100-more" style="background-image: url('Assets/images/bg-01.jpg');  " ></div>


			<div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
				<form class="login100-form validate-form" method = "POST" enctype = "multipart/form-data"  action = "<?php echo $_SERVER["PHP_SELF"]; ?>">
					<span class="login100-form-title p-b-59">
						Sign Up
					</span>


					<div class="wrap-input100 validate-input" >
						<span class="label-input100">First Name</span>
						<input class="input100" type="text" name="fname" placeholder="FirstName...">
						<span class="focus-input100"></span>
						<?php
							if(isset($sign_up_errors["fname"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_up_errors["fname"]."</font>"."</span>";
							}
						?>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Last Name</span>
						<input class="input100" type="text" name="lname" placeholder="LastName...">
						<span class="focus-input100"></span>
						<?php
							if(isset($sign_up_errors["lname"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_up_errors["lname"]."</font>"."</span>";
							}
						?>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Job</span>
						<input class="input100" type="text" name="job" placeholder="Job...">
						<span class="focus-input100"></span>
						<?php
							if(isset($sign_up_errors["job"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_up_errors["job"]."</font>"."</span>";
							}
						?>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Email</span>
						<input class="input100" type="text" name="email" placeholder="Email addess...">
						<span class="focus-input100"></span>
						<?php
							if(isset($sign_up_errors["email"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_up_errors["email"]."</font>"."</span>";
							}
						?>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Username</span>
						<input class="input100" type="text" name="username" placeholder="Username...">
						<span class="focus-input100"></span>
						<?php
							if(isset($sign_up_errors["username"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_up_errors["username"]."</font>"."</span>";
							}
						?>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" placeholder="*************">
						<span class="focus-input100"></span>
						<?php
							if(isset($sign_up_errors["password"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_up_errors["password"]."</font>"."</span>";
							}
						?>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="label-input100">Repeat Password</span>
						<input class="input100" type="password" name="repeat-password" placeholder="*************">
						<span class="focus-input100"></span>
						<?php
							if(isset($sign_up_errors["repeat-password"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_up_errors["repeat-password"]."</font>"."</span>";
							}
						?>
					</div>
					
					<div class="box">
						<input type="file" name="image" id="image" class="inputfile inputfile-1"  />
						<label for="image"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>image&hellip;</span></label>

						<?php
						
							if(isset($sign_up_errors["image"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_up_errors["image"]."</font>"."</span>";
							}
						
						?>
					</div>

					<div class="box">
						<input type="file" name="cv" id="cv" class="inputfile inputfile-1"  />
						<label for="cv"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>CV&hellip;</span></label>
						<?php
						
							if(isset($sign_up_errors["cv"])){
								echo "<span style = color:red >"."<font size = 2>" .$sign_up_errors["cv"]."</font>"."</span>";
							}
						
						?>
					</div>


					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button type = "submit" name = "signup" class="login100-form-btn">
								Sign Up
							</button>
						</div>

					</div>


				</form>

			</div>
		</div>
	</div>



</body>
</html>