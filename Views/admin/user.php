<?php 
	defined("_ALLOW_ACCESS") or die("Access Not Allowed");

    $userId = $show_user->get_user_id($_GET["id"]);


    $userData = $show_user->get_user_data($userId);
    $userName = $show_user->get_user_name($userData);
    $userJob = $show_user->get_user_job($userData);
    $userEmail = $show_user->get_user_email($userData);
    $userImage = $show_user->get_user_image($userData);
    $userCv = $show_user->get_user_cv($userData);


?>


<!DOCTYPE HTML>
<html>
<head>
    <link href="Assets/profile/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="Assets/profile/js/jquery.min.js"></script>
    <!-- Custom Theme files -->
    <link href="Assets/profile/css/style.css" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Preface Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <!-- webfonts -->
    <link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <!-- webfonts -->
    <script type="text/javascript" src="Assets/profile/js/move-top.js"></script>
    <script type="text/javascript" src="Assets/profile/js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
</head>
<body>
<!-- container -->
<!-- header -->
<div id="home" class="header"  >
    <script>

        let url = '<?php

            echo "url($userImage)"  ;

            ?>';
        let image = document.querySelector("#home");
        image.style.background = url;
        image.style.backgroundRepeat = "repeat-n";
        image.style.backgroundPosition = "center top";
        image.style.backgroundSize = "cover";
        image.style.webkitBackgroundSize = "cover";
        image.style.minHeight = "100vh";
        image.style.position = "relative"
    </script>
    <div class="container">
        <!-- top-hedader -->

        <div class="top-header">
            <!-- /logo -->
            <!--top-nav---->
            <div class="top-nav">
                <div class="navigation">
                    <div class="logo">
                        <h1><a href=""><span>R</span>ESUME</a></h1>
                    </div>
                    <div class="navigation-right">
                        <span class="menu"><img src="images/menu.png" alt=" " /></span>
                        <nav class="link-effect-3" id="link-effect-3">
                            <ul class="nav1 nav nav-wil">
                                <li><a  data-hover="View All" href="
                                <?php

                                    echo $_SERVER["PHP_SELF"]."?home";
                                ?>">View All</a></li>
                                <li><a  data-hover="Logout" href="<?php echo $_SERVER["PHP_SELF"]."?logout"; ?>">Logout</a></li>
                            </ul>
                        </nav>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /top-hedader -->
            </div>
        <!-- /top-hedader -->
    </div>
    <div class="banner-info">
        <div class="col-md-7 header-right">
            <h1>Hi !</h1>
            <h6><?php echo $userName;?></h6>
            <ul class="address">

                <li>
                    <ul class="address-text">
                        <li><b>NAME</b></li>
                        <li><?php echo $userName;?></li>
                    </ul>
                </li>

                <li>
                    <ul class="address-text">
                        <li><b>JOB</b></li>
                        <li><?php echo $userJob;?></li>
                    </ul>
                </li>

                <li>
                    <ul class="address-text">
                        <li><b>E-MAIL </b></li>
                        <li><?php echo $userEmail;?></li>
                    </ul>
                </li>
                <li>
                    <ul class="address-text">
                        <li><b>CV </b></li>
                        <li><?php echo "<a href=$userCv target='_blank'><span>View CV</span></a>";?></li>
                    </ul>
                </li>

            </ul>
        </div>
        <div class="col-md-5 header-left">
            <?php echo "<img src=$userImage width='250' height='300'>";?>
        </div>
        <div class="clearfix"> </div>

    </div>
</div>
</div>
<!-- //for bootstrap working -->
<script src="Assets/profile/js/bootstrap.js"></script>

</body>
</html>
