<?php


    defined("_ALLOW_ACCESS") or die("Access Not Allowed");


    //get the first 5 in the database when u first log in then calculate depending on the next
    if(isset($_GET["next"]) && is_numeric($_GET["next"])){
        $current_index = (int)$_GET["next"];

    }
    //get the first 5 in the database when u first log in then calculate depending on the next
    elseif(isset($_GET["previous"]) && is_numeric($_GET["previous"])){

        $current_index = (int)$_GET["previous"];

    }else{

        $current_index = 1;

    }

    //first 5 rows
    $arr = $db->show_all_data($current_index);

    //next 5 rows
    $next_arr = $db->show_all_data($current_index + __RECORDS_PER_PAGE__);

    //if there are next 5 rows then display them
    if($next_arr){


        $next_index = $current_index + __RECORDS_PER_PAGE__;
        $previous_index = ($current_index - __RECORDS_PER_PAGE__ > 0) ? $current_index - __RECORDS_PER_PAGE__ : 1 ;

    }
    //else don't move from your place
    else{
        
        $next_index = $current_index;
        $previous_index = ($current_index - __RECORDS_PER_PAGE__ > 0) ? $current_index - __RECORDS_PER_PAGE__ : 1 ;

    }


    

    
?>



<!DOCTYPE html>
<html lang="en">
<head>
	<title></title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="Assets/css/users.css">
    

</head>
<body >

<div class="container">

        <div class="top-header">

            <div class="top-nav">
                <div class="navigation">
                    <div class="navigation-right">
                        <nav class="link-effect-3" id="link-effect-3">
                            <ul class="nav1 nav nav-wil" style="list-style: none;">
                                <li><a id = "logout" data-hover="Logout" href="<?php echo $_SERVER["PHP_SELF"]."?logout"; ?>">Logout</a></li>
                            </ul>
                        </nav>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
    </div>

<table class="container">
	<thead>
		<tr>
			<th><h1>Name</h1></th>
			<th><h1>UserName</h1></th>
			<th><h1>Job</h1></th>
			<th><h1>View Profile</h1></th>
		</tr>
	</thead>
	<tbody>

        <?php
            foreach($arr as $row){

                echo "<tr> <td>".$row["firstname"]." ".$row["lastname"]."</td>";
                echo "<td>".$row["username"]."</td>";
                echo "<td>".$row["job"]."</td>";
                echo "<td><a href = ".$_SERVER["PHP_SELF"]."?id=".$row["id"].">View Profile</a> </td> </tr>";
            } 
    
    
        ?>
	</tbody>
</table>

<ul class="nav1 nav nav-wil" style="list-style: none;">
    <li> <a  id = "prev" href="<?php echo $_SERVER["PHP_SELF"]."?previous=".$previous_index; ?>" > Prev </a></li>
</ul>

<ul class="nav1 nav nav-wil" style="list-style: none;">
    <li><a id = "nex" href="<?php echo $_SERVER["PHP_SELF"]."?next=".$next_index; ?>" >Next</a></li>
</ul>

	


</body>
</html>
