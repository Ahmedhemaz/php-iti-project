
<?php

defined("_ALLOW_ACCESS") or die("Access Not Allowed");


$userId = $_SESSION["user_id"];

$userData = $db->show_data_by_id($userId);
$userFullName = $userData[0]['firstname']."  ".$userData[0]['lastname'];
$userJob = $userData[0]['job'];
$userName = $userData[0]['username'];
$userFirstName = $userData[0]['firstname'];
$userLastName = $userData[0]['lastname'];
$userEmail = $userData[0]['email'];
$userImage = __image_dir__.$userData[0]['image_name'];
$userCv = __cv_dir__.$userData[0]['cv_name'];

if(isset($_POST["edit"])){

    echo "edit";
    if(isset($_POST['fname'])){
        $db->updateData($_POST['fname'],$userId,"firstname");
    }
    if(isset($_POST['lname'])){
        $db->updateData($_POST['lname'],$userId,"lastname");
    }
    if(isset($_POST['email'])){
        $db->updateData($_POST['email'],$userId,"email");
    }
    if(isset($_POST['username'])){
        $db->updateData($_POST['username'],$userId,"username");
    }
    if(isset($_POST["job"])){
        $db->updateData($_POST['job'],$userId,"job");
    }

    if($_FILES["image"]["name"] != ""){
        $image_name = basename($_FILES["image"]["name"]);
        $image_file_extension = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
        $target_image_path = __image_dir__.$image_name;
        move_uploaded_file($_FILES["image"]["tmp_name"] , $target_image_path);
        $db->updateData($image_name,$userId,"image_name");
    }

    if($_FILES["cv"]["name"] != ""){
        $cv_name = basename($_FILES["cv"]["name"]);
        $cv_file_extension = pathinfo($_FILES["cv"]["name"], PATHINFO_EXTENSION);
        $target_cv_path = __cv_dir__.$cv_name;
        move_uploaded_file($_FILES["cv"]["tmp_name"] , $target_cv_path);
        $db->updateData($cv_name,$userId,"cv_name");
    }

    header('Refresh:0');

}



?>



<!DOCTYPE HTML>
<html>
<head>
    <link href="Assets/profile/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="Assets/profile/js/jquery.min.js"></script>
    <!-- Custom Theme files -->
    <link href="Assets/profile/css/style.css" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Preface Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <!-- webfonts -->
    <link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <!-- webfonts -->
    <script type="text/javascript" src="Assets/profile/js/move-top.js"></script>
    <script type="text/javascript" src="Assets/profile/js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
</head>
<body>
<!-- container -->
<!-- header -->
<div id="home" class="header"  >
    <script>

        let url = '<?php

            echo "url($userImage)"  ;

            ?>';
        let image = document.querySelector("#home");
        image.style.background = url;
        image.style.backgroundRepeat = "repeat-n";
        image.style.backgroundPosition = "center top";
        image.style.backgroundSize = "100vh";
        image.style.webkitBackgroundSize = "cover";
        image.style.minHeight = "100vh";
        image.style.position = "relative"
    </script>
    <div class="container">
        <!-- top-hedader -->

        <div class="top-header">
            <!-- /logo -->
            <!--top-nav---->
            <div class="top-nav">
                <div class="navigation">
                    <div class="logo">
                        <h1><a href=""><span>R</span>ESUME</a></h1>
                    </div>
                    <div class="navigation-right">
                        <span class="menu"><img src="images/menu.png" alt=" " /></span>
                        <nav class="link-effect-3" id="link-effect-3">
                            <ul class="nav1 nav nav-wil">
                                <li><a  data-hover="About" href="
                                <?php

                                    echo $_SERVER["PHP_SELF"]."?about";
                                ?>">About</a></li>
                                <li><a  data-hover="Edit" href="
                                <?php
                                    echo $_SERVER["PHP_SELF"]."?edit";
                                  ?>">Edit</a></li>
                                <li><a  data-hover="Logout" href=<?php echo $_SERVER["PHP_SELF"]."?logout"; ?>>Logout</a></li>
                            </ul>
                        </nav>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /top-hedader -->
            </div>
            <!-- /top-hedader -->
        </div>
        <form method="post"  enctype="multipart/form-data">
            <div class="banner-info" style="margin-top: 15px;  padding-bottom: 50px;">
                <div class="col-md-7 header-right">
                    <h1>Hi !</h1>
                    <h6><?php echo $userFullName;?></h6>
                    <ul class="address">
                        <li>
                            <ul class="address-text">
                                <li><b>FIRST NAME</b></li>
                                <li><input type="text" class="form-control" value=<?php echo $userFirstName;?> name="fname" id="inputFirstName4" placeholder="FirstName "></li>
                            </ul>
                        </li>
                        <li>
                            <ul class="address-text">
                                <li><b>LAST NAME</b></li>
                                <li><input type="text" class="form-control" value="<?php echo $userLastName;?>" name="lname" id="inputLastName4" placeholder="LastName"></li>
                            </ul>
                        </li>

                        <li>
                            <ul class="address-text">
                                <li><b>JOB</b></li>
                                <li><input type="text" class="form-control" value=<?php echo $userJob;?> name="job" id="inputJob4" placeholder="Job"></li>
                            </ul>
                        </li>

                        <li>
                            <ul class="address-text">
                                <li><b>E-MAIL </b></li>
                                <li><input type="email" class="form-control" value=<?php echo $userEmail;?> name="email" id="inputEmail4" placeholder="E-mail"></li>
                            </ul>
                        </li>

                        <li>
                            <ul class="address-text">
                                <li><b>CV </b></li>
                                <li>
                                    <label class="btn btn-default btn-file" style=" color: white; background-color: #00a78e;">
                                        Upload <input type="file" name="cv" style="display: none;  background-color: #00a78e;">
                                    </label>
                                </li>
                            </ul>
                        </li>

                    </ul>
                    <button type="submit" class="btn btn-primary btn-lg" name="edit" style="  background-color: #00a78e;">Submit</button>

                </div>
                <div class="col-md-5 header-left" style="margin-top: 10%; right: 10%;">
                    <?php echo "<img src=$userImage >";?>
                    <label class="btn btn-default btn-file" style="  color: white; background-color: #00a78e; margin-left: 30%;: 100px;">
                        Change Image <input type="file" name="image" style="display: none; ">
                    </label>
                </div>

                <div class="clearfix"> </div>

            </div>
        </form>
    </div>
</div>
<!-- //for bootstrap working -->
<script src="Assets/profile/js/bootstrap.js"></script>

</body>
</html>
